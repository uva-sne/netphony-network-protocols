package es.tid.pce.pcep.objects.tlvs;

import es.tid.pce.pcep.objects.MalformedPCEPObjectException;
import es.tid.pce.pcep.objects.ObjectParameters;
import es.tid.protocol.commons.ByteHandler;

/**
 * TE-PATH-BINDING (Type 55)
 */


public class PathBindingTLV extends PCEPTLV 
{
	/*
	 *  * The TE-PATH-BINDING  TLV is an optional TLV for use in the
   LSP Object for binding SID.  
   Its format is
   shown in the following figure:

       0                   1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |           Type = 55           |             Length            |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |      BT       |    Flags      |            Reserved           |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      ~            Binding Value (variable length)                    ~
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


                       Figure 2: TE-PATH-BINDING TLV

   TE-PATH-BINDING TLV is a generic TLV such that it is able to carry
   binding label/SID (i.e.  MPLS label or SRv6 SID).  It is formatted
   according to the rules specified in [RFC5440].  The value portion of
   the TLV comprises:

   Binding Type (BT): A one-octet field identifies the type of binding
   included in the TLV.  This document specifies the following BT
   values:

   o  BT = 0: The binding value is a 20-bit MPLS label value.  The TLV
      is padded to 4-bytes alignment.  The Length MUST be set to 7 and
      the first 20 bits are used to encode the MPLS label value.

   o  BT = 1: The binding value is a 32-bit MPLS label stack entry as
      per [RFC3032] with Label, TC [RFC5462], S, and TTL values encoded.
      Note that the receiver MAY choose to override TC, S, and TTL
      values according to its local policy.  The Length MUST be set to
      8.

   o  BT = 2: The binding value is an SRv6 SID with a format of a
      16-octet IPv6 address, representing the binding SID for SRv6.  The
      Length MUST be set to 20.

   o  BT = 3: The binding value is a 24 octet field, defined in
      Section 4.1, that contains the SRv6 SID as well as its Behavior
      and Structure.  The Length MUST be set to 28.
	 */
	protected byte BT;
	protected int SID;
	
	public PathBindingTLV(){
		this.TLVType=65505;
		this.BT = 0;
		this.SID =0;
	}

	public PathBindingTLV(byte[] bytes, int offset)throws MalformedPCEPObjectException{		
		super(bytes,offset);		
		decode();
	}

	@Override
	public void encode() 
	{		
		log.debug("Encoding PathBindingTLV");
		int length=8;
		if(this.BT == 2)
			length = 20;
		if(this.BT == 3)
			length = 28;
		this.setTLVValueLength(length);
		this.tlv_bytes=new byte[this.getTotalTLVLength()];
		encodeHeader();
		
		int Zero = 0;
		int offset = 4;
		this.tlv_bytes[offset] = this.BT;
		offset = 8;
		this.tlv_bytes[6]=(byte)(SID >>> 12 & 0xff);
		this.tlv_bytes[7]=(byte)(SID >>> 4 & 0xff);
		this.tlv_bytes[8]=(byte)((SID & 0x0F) << 4);
	}
	
	public void decode()
	{
	}
	
	//GETTERS & SETTERS
	
	public byte BT() 
	{
		return BT;
	}
	public void setBT(byte BT) 
	{
		this.BT=BT;
	}

	public int SID() 
	{
		return SID;
	}
	public void setSID(int SID) 
	{
		this.SID=SID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + BT;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PathBindingTLV other = (PathBindingTLV) obj;
		if (BT != other.BT)
			return false;
		return true;
	}

	
}
